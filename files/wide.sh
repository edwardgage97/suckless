#!/bin/sh

cvt 2560 1080 120

xrandr --newmode "2560x1080"  492.00  2560 2768 3048 3536  1080 1083 1093 1160 -hsync +vsync
xrandr --addmode Virtual-1 "2560x1080"
xrandr -s 2560x1080

feh --no-fehbg --bg-fill ~/.config/suckless/files/dwmlogo.png

clear
