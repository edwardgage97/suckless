#!/bin/sh
clear
read -p "laptop or desktop? l/d: " pctype
read -p "runit/systemd? r/s: " init
read -p "has this script been run before? y/enter: " scriptran
if [ $scriptran = y ]; then
	clear
else
	if [ -e ~/.config/suckless ]; then
		read -p "update dwm? y/n: " update
		if [ $update = y ]; then
			doas rm -r ~/.config/suckless
		fi
	fi
	if [ -e ~/suckless ]; then
		sudo mv ~/suckless ~/.config
	fi
sudo chown -R $(whoami):root ~/.config/suckless
sudo chmod -R 771 ~/.config/suckless
sudo chmod -R 644 ~/.config/suckless/files
sudo chmod 771 ~/.config/suckless/files
sudo pacman -Syu; clear
#dependencies
yes " " | sudo pacman -S base-devel git vim arc-gtk-theme mint-y-icons rsync rsync-runit doas; clear
sudo mv ~/.config/suckless/files/doas.conf /etc && sudo chmod 755 /etc/doas.conf && sudo chown root:root /etc/doas.conf
#audio
yes "y" | doas pacman -S pipewire pipewire-pulse wireplumber; clear
#Xutil
yes " " | doas pacman -S slock screengrab; clear
doas pacman -S picom
#suckless
cd ~/.config/suckless/dwm; doas make clean install; chmod 777 autostart.sh; clear
cd ../dmenu; doas make clean install; clear
cd ../st; doas make clean install; clear
cd ../slstatus; doas make clean install; clear
cd ../..; git clone https://aur.archlinux.org/ttf-twemoji.git && cd ttf-twemoji && makepkg -si && cd .. && doas rm -r ttf-twemoji; clear
if [ $pctype = l ]; then
	yes " " | doas pacman -S tlp tlp-rdw tlp-runit python-gobject python-setuptools brightnessctl acpid
	git clone https://aur.archlinux.org/tlpui.git && cd tlpui && makepkg -si && cd .. && doas rm -r tlpui
fi
doas pacman -S element-desktop; clear
doas pacman -S firefox; clear
doas pacman -S gnome-multi-writer; clear
#config
cd ~/.config/suckless/files
doas chmod 771 settings.ini
doas chmod 755 neofetch
doas chmod 775 galculator htop libreoffice screengrab
doas chown root:root pacman.conf neofetch
mkdir -p ~/.config/picom && doas cp -a picom.conf ~/.config/picom
doas cp -a xinitrc /etc/X11/xinit
doas cp -a settings.ini ~/.config/gtk-3.0
if [ $pctype = l ]; then
	doas cp -a tlp.conf /etc
	doas ln -s /etc/runit/sv/tlp /etc/runit/runsvdir/default
fi
clear
#random
yes " " | doas pacman -S thunar thunar-volman thunar-media-tags-plugin gvfs thunar-archive-plugin engrampa unrar p7zip raw-thumbnailer ristretto vlc mpv feh; clear
#fonts
yes " " | doas pacman -S ttf-liberation ttf-ubuntu-font-family woff-fira-code woff2-fira-code ttf-font-awesome ttf-croscore gnu-free-fonts gsfonts libotf noto-fonts noto-fonts-emoji noto-fonts-extra ttf-carlito ttf-bitstream-vera; clear
#apps
yes " " | doas pacman -S htop gparted gnome-disk-utility dosfstools mtools
yes " " | doas pacman -Rns nano xcursor-premium epiphany
clear
#other
yes " " | doas pacman -S gufw ufw ufw-runit
doas ln -s /etc/runit/sv/ufw /etc/runit/runsvdir/default
doas sv up ufw
doas pacman -S meson
doas pacman -S libreoffice-fresh galculator
fi
#xdg
clear; read -p "remove xdg dirs capitals? (Videos->videos) y/enter: " xdg
if [ $xdg = y ]; then
	cd ~/
	cp -a ~/.config/suckless/files/user-dirs.dirs ~/.config
	mkdir music desktop documents downloads pictures public templates videos
	rsync -aP Music/ music; rsync -aP Desktop/ desktop; rsync -aP Documents/ documents; rsync -aP Downloads/ downloads
       	rsync -aP Pictures/ pictures; rsync -aP Public/ public; rsync -aP Templates/ templates; rsync -aP Videos/ videos
	doas rm -r Music Desktop Documents Downloads Pictures Public Templates Videos
fi
clear; read -p "create media directories? (/media/usb, etc.) y/enter: " mediadir
if [ $mediadir = y ]; then
	doas mkdir -p /media/usb && cd /media && doas mkdir files sd msd usb2
	if [[ "$(df -hT | grep /$)" == */dev/sd* || "$(df -hT | grep /$)" == */dev/nvme* || "$(df -hT | grep /$)" == */dev/luks* ]]; then
		doas mkdir luks luks2 ventoy iso qcow ssd hdd
	fi
	doas chmod -R 775 /media && doas chown -R $(whoami):root /media
fi
#vm
if [[ "$(df -hT | grep /$)" == */dev/sd* || "$(df -hT | grep /$)" == */dev/nvme* || "$(df -hT | grep /$)" == */dev/luks* ]]; then
	clear; read -p "install virt-manager? y/enter: " virtman
	if [ $virtman = y ]; then
		doas pacman -S virt-manager qemu libvirt libvirt-runit edk2-ovmf dnsmasq vde2 bridge-utils openbsd-netcat iptables-nft dmidecode
		doas ln -s /etc/runit/sv/libvirtd /etc/runit/runsvdir/default; doas ln -s /etc/runit/sv/virtlogd /etc/runit/runsvdir/default
		doas sv up libvirtd; doas sv up virtlogd
		doas cp -a ~/.config/suckless/files/libvirtd.conf /etc/libvirt
		doas usermod -aG libvirt $(whoami)
		read -p "uncomment and change user and group to $(whoami) | press enter to continue "
		doas vim /etc/libvirt/qemu.conf +519
	fi
fi
if [[ "$(df -hT | grep /$)" == */dev/vd* ]]; then
	cd ~/.config/suckless
	doas chmod 777 files/wide.sh; doas chmod 777 files/autostart.sh
	cp -a files/wide.sh .; cp -a files/autostart.sh dwm
fi
#grub
clear; read -p "customize grub theming/select-time? t/s/b/enter: " grub
if [ $grub = t -o $grub = b ]; then
	doas rm -r /usr/share/grub/themes/artix
	doas grub-mkconfig -o /boot/grub/grub.cfg; clear
fi
if [ $grub = s -o $grub = b ]; then
	doas vim /etc/default/grub +2
	doas grub-mkconfig -o /boot/grub/grub.cfg
	read -p "press enter to continue "; clear
fi
#branding
clear; read -p "remove artix branding? y/enter: " branding
if [ $branding = y ]; then
	doas pacman -Rns artix-branding
	yes " " | doas pacman -S neofetch
fi
#configs
clear; read -p "customize suckless? y/enter: " suckconfig
if [ $suckconfig = y ]; then
	cd ~/.config/suckless/dwm
	clear; read -p "customize dwm? y/enter: " dwmconfig
	if [ $dwmconfig = y ]; then
		cd ../dwm
		vim config.h && read -p "did you make changes? y/enter: " changes
		if [ $changes = y ]; then
			doas make clean install
		fi
	fi
	clear; read -p "customize slstatus? y/enter: " statconfig
	if [ $statconfig = y ]; then
		cd ../slstatus
		vim config.h && read -p "did you make changes? y/enter: " changes
		if [ $changes = y ]; then
			doas make clean install
		fi
	fi
	clear; read -p "customize dmenu? y/enter: " dmenuconfig
	if [ $dmenuconfig = y ]; then
		cd ../dmenu
		vim config.h && read -p "did you make changes? y/enter: " changes
		if [ $changes = y ]; then
			doas make clean install
		fi
	fi
	clear; read -p "customize st? y/enter: " stconfig
	if [ $stconfig = y ]; then
		cd ../st
		vim config.h && read -p "did you make changes? y/enter: " changes
		if [ $changes = y ]; then
			doas make clean install
		fi
	fi
fi
if [ -e /usr/bin/neofetch ]; then
	clear; read -p "customize neofetch? (preconfigured/manual) p/m/enter: " neocfg
	if [ $neocfg = p ]; then
		doas cp -a ~/.config/suckless/files/neofetch /usr/bin
	fi
	if [ $neocfg = m ]; then
		doas vim /usr/bin/neofetch +59; clear
	fi
fi
if [ -e /usr/bin/vim ]; then
	clear; read -p "customize vim? (preconfigured/manual) p/m/enter: " vimcfg
	if [ $vimcfg = p ]; then
		doas cp -a ~/.config/suckless/files/vimrc /etc
	fi
	if [ $vimcfg = m ]; then
		read -p "look up 'customizing vim' for info | press enter to continue "
		doas vim /etc/vimrc +11; clear
	fi
fi
if [ -e /usr/bin/galculator ]; then
	clear; read -p "customize galculator? (preconfigured/manual) p/m/enter: " calccfg
	if [ $calccfg = p ]; then
		doas cp -a ~/.config/suckless/files/galculator ~/.config
	fi
	if [ $calccfg = m ]; then
		vim ~/.config/galculator/galculator.conf
	fi
fi
if [ -e /usr/bin/htop ]; then
	clear; read -p "customize htop? (preconfigured/manual) p/m/enter: " htopcfg
	if [ $htopcfg = p ]; then
		doas cp -a ~/.config/suckless/files/htop ~/.config
	fi
	if [ $htopcfg = m ]; then
		vim ~/.config/htop/htoprc
	fi
fi
if [ -e /usr/bin/libreoffice ]; then
	clear; read -p "customize libreoffice? (preconfigured) p/enter: " offcfg
	if [ $offcfg = p ]; then
		doas cp -a ~/.config/suckless/files/libreoffice ~/.config
	fi
fi
if [ -e /usr/bin/screengrab ]; then
	clear; read -p "customize screengrab? (preconfigured/manual) p/m/enter: " scrcfg
	if [ $scrcfg = p ]; then
		doas cp -a ~/.config/suckless/files/screengrab ~/.config
	fi
	if [ $scrcfg = m ]; then
		vim ~/.config/screengrab/screengrab.conf
	fi
fi
clear; read -p "customize pacman.conf? (preconfigured/manual) p/m/enter: " pacmancfg
if [ $pacmancfg = p ]; then
	doas cp -a ~/.config/suckless/files/pacman.conf /etc
	doas pacman -Syu
fi
if [ $pacmancfg = m ]; then
	doas vim /etc/pacman.conf
	doas pacman -Syu
fi
#bash
clear; read -p "customize bashrc? (preconfigured/manual) p/m/enter: " bashcfg
if [ $bashcfg = p ]; then
		doas cp -a ~/.config/suckless/bash/.bashrc ~/
fi
if [ $bashcfg = m ]; then
		vim ~/.bashrc
fi
clear; read -p "add included bash scripts? y/enter: " bashscr
if [ $bashscr = y ]; then
	doas rm -r ~/bash; cp -a ~/.config/suckless/bash ~/
fi
if [ -e ~/bashrc.d ]; then
	clear; ls ~/ | grep bashrc.d
	ls ~/bashrc.d
	read -p "delete this folder? (needed for backup) y/enter: " delperm
	if [ $delperm = y ]; then
		doas rm -r ~/bashrc.d
		doas cp -a /etc/bash/bashrc.d ~/
	fi
else
	doas cp -a /etc/bash/bashrc.d ~/
fi
clear; read -p "change bash theming? (artix/local) a/l/enter: " bashtheming
if [ $bashtheming = a ]; then
	doas cp -a ~/.config/suckless/files/artix.bashrc /etc/bash/bashrc.d
	doas rm /etc/bash/bashrc.d/local.bashrc
fi
if [ $bashtheming = l ]; then
	doas rm /etc/bash/bashrc.d/artix.bashrc
fi
if [ $bashtheming = a -o $bashtheming = l ]; then
	if [ -e ~/bashrc.d ]; then
		clear; read -p "revert changes? y/enter: " revert
		if [ $revert = y ]; then
			doas rm -r ~/etc/bash/bashrc.d
			doas mv ~/bashrc.d /etc/bash
		else
			doas rm -r ~/bashrc.d
		fi
	fi
fi
#browser
clear; read -p "add included firefox config? y/enter: " ffconfig
if [ $ffconfig = y ]; then
	doas rm -r ~/.mozilla
	doas cp -a ~/.config/suckless/browser/.mozilla ~/
fi
#services
clear; read -p "configure services? (add/remove) a/r/b/enter: " servicecfg
while [ $servicecfg = a -o $servicecfg = b ]; do
	clear; echo "available services"; ls --color=auto -A /etc/runit/sv
	echo "running services"; ls --color=auto -a /etc/runit/runsvdir/default
	read -p "which services do you want to add? service name/n: " service
	if [ $service = n ]; then
		clear; break
	else
		doas ln -s /etc/runit/sv/$answer /etc/runit/runsvdir/default
		doas sv up $answer
	fi
done
if [ $servicecfg = r -o $servicecfg = b ]; then
	read -p "dont remove your display manager until you are ready to reboot | press enter to continue "
fi
while [ $servicecfg = r -o $servicecfg = b ]; do
	clear; echo "running services"; ls --color=auto -A /etc/runit/runsvdir/default
	read -p "which services do you want to remove? service name/n: " service
	if [ $service = n ]; then
		clear; break
	else
		doas rm -r /etc/runit/runsvdir/default/$service
		clear
	fi
done
#de
if [[ "$(doas pacman -Qs plasma)" == *plasma* || "$(doas pacman -Qs xfce4)" == *xfce4* || "$(doas pacman -Qs lxde)" == *lxde* ||"$(doas pacman -Qs lxqt)" == *lxqt* ||"$(doas pacman -Qs mate)" == *mate* ||"$(doas pacman -Qs cinnamon)" == *cinnamon* ]]; then
clear; read -p "remove included DE apps? DE name (kde, xfce, lxde, lxqt, mate, cinnamon, other)/enter: " de
	if [ $de = kde ]; then
		yes " " | doas pacman -Rns plasma kde-applications; clear
	fi
	if [ $de = xfce ]; then
		clear; read -p "remove lightdm? (you will reboot) y/enter: " dm_rm
		yes " " | doas pacman -Rns xfce4 xfce4-goodies leafpad
		if [ $dm_rm = y ]; then
			doas pacman -Rns lightdm
			doas rm -r /etc/runit/runsvdir/default/lightdm
			doas reboot
		fi
	fi
	if [ $de = lxde ]; then
		yes " " | doas pacman -Rns lxde; clear
	fi
	if [ $de = lxqt ]; then
		yes " " | doas pacman -Rns lxqt; clear
	fi
	if [ $de = mate ]; then
		 yes " " | doas pacman -Rns mate mate-extra; clear
	fi
	if [ $de = cinnamon ]; then
		 yes " " | doas pacman -Rns cinnamon; clear
	fi
	if [ $de = other ]; then
		read -p "there is no support for this DE | press enter to continue "
	fi
fi
#suckless-reboot
if [ $changes = y ]; then
	read -p "rebooting to load changes to suckless tools | press enter to continue | press ctrl+c to cancel "
	doas reboot
fi
clear
