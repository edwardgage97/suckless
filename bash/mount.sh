#!/bin/sh
choice="y"
clear

while [ $choice = y ]; do
	clear; sudo fdisk -l
	read -p "
which device do you want to mount?
(ex: sda1) or type n to exit: " drive
	if [ $drive = n ]; then
		clear; break; exit 0
	fi
	read -p "is the device luks-encrypted? y/n: " luks
	clear; read -p "where do you want the device mounted?
	(ex: /media/backups): " mp
	if [ $luks = y ]; then
		sudo cryptsetup open /dev/$drive $drive; clear
		read -p "do you have other luks-encrypted devices mounted? y/n: " other
		if [ $other = y ]; then
			clear; read -p "how many? 1, 2, 3, etc: " amount
		fi
		if [ $other = n ]; then
			amount="0"
		fi
		sudo mkdir -p $mp; sudo chown -R $(whoami):root $mp; chmod -R 775 $mp
		sudo mount /dev/dm-$amount $mp; clear
	fi
	if [ $luks = n ]; then
		sudo mount /dev/$drive
	fi
	read -p "do you want to mount another device? y/n: " choice
done
if [ $choice = n ]; then
	clear; exit 0
fi
