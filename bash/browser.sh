#!/bin/sh
clear

#Mounting
read -p "do you want to mount a device? y/n: " choice
if [ $choice = y ]; then
	~/bash/mount.sh
fi
clear; read -p "are you backing up or loading configs? b/l: " configs
clear
#Loading
if [ $configs = l ]; then
	read -p "do you want to load a firefox config? y/n: " fire
	if [ $fire = y ]; then
		read -p "where is the config located?
		(ex: /home/$(whoami)/browser): " fire
		sudo rm -r ~/.var/app/org.mozilla.firefox/.mozilla
		sudo rsync -a --partial $fire/.mozilla ~/.var/app/org.mozilla.firefox; clear
	fi
	read -p "do you want to load a librewolf config? y/n: " wolf
	if [ $wolf = y ]; then
		read -p "where is the config located?
		(ex: /home/$(whoami)/browser): " wolf
		sudo rm -r ~/.var/app/io.gitlab.librewolf-community/.librewolf
		sudo rsync -a --partial $wolf/.librewolf ~/.var/app/io.gitlab.librewolf-community; clear
	fi
fi
#Backing-up
if [ $configs = b ]; then
	#mozilla
	read -p "
	copy firefox config? y/n: " answer
	if [ $answer = y ]; then
		read -p "
		where do you want it saved to?
		(ex: /home/$(whoami)): " path
		sudo rm -r $path/browser/.mozilla; sudo mkdir -p $path/browser
		sudo rsync -a --partial ~/.var/app/org.mozilla.firefox/.mozilla $path/browser
		clear; ls --color=auto -a $path/browser
	fi
	#librewolf
	read -p "
	copy librewolf config? y/n: " answer
	if [ $answer = y ]; then
		read -p "where do you want it saved to?
		(ex: /home/$(whoami)): " path
		sudo rm -r $path/browser/.librewolf; sudo mkdir -p $path/browser
		sudo rsync -a --partial ~/.var/app/io.gitlab.librewolf-community/.librewolf $path/browser
		clear; ls --color=auto -a $path/browser
	fi
fi
read -p "press enter to continue"
clear
