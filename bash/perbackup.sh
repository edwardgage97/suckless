#!/bin/sh
clear

#VARIABLES
line="==================="
menu="    Backup Menu    "

#DOCUMENTS
sudo rsync -ar ~/Documents /media/MS/backups/per /media/NSB/backups/per
clear

#DOWNLOADS
sudo rsync -ar ~/Downloads /media/MS/backups/per /media/NSB/backups/per
clear

#PICTURES
sudo rsync -ar ~/Pictures /media/MS/backups/per /media/NSB/backups/per
clear

#VIDEOS
sudo rsync -ar ~/Videos /media/MS/backups/per /media/NSB/backups/per
clear

#OTHER
echo "
$line
$menu Personal-Backup
$line
"
read -p "do you want to backup to another location? type the exact file path or type no: " path
if [ $path = no ]; then
	clear; exit 0
else
	sudo mkdir -p $path
	sudo rsync -ar ~/Documents $path
	sudo rsync -ar ~/Downloads $path
       	sudo rsync -ar ~/Pictures $path 
	sudo rsync -ar ~/Videos $path
	~/bash/perloop.sh
fi
