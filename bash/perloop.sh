#!/bin/bash
clear

#variables
line="==================="
menu="    Backup Menu    "

echo "
$line
$menu Personal-Backup
$line
"
read -p "do you want to backup to another location? type the exact file path or type no: " path
if [ $path = no ]; then
	clear; exit 0
else
	sudo mkdir -p $path
	sudo rsync -ar ~/Documents $path
	sudo rsync -ar ~/Downloads $path
	sudo rsync -ar ~/Pictures $path
	sudo rsync -ar ~/Videos $path
	~/bash/perloop.sh
fi
