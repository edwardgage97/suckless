#/bin/sh

#git-info
clear; read -p "enter your git client username: " user
clear; read -p "enter your git client email: " email
git config --global user.name "$user"
git config --global user.email "$email"
clear; read -p "enter your repo name: " repo
clear; read -p "enter the branch you are editing: " branch
if [ ! $(pwd) = /home/$(whoami)/.config ]; then
	sudo rm -r $repo
fi
git clone https://gitlab.com/$user/$repo
cd $repo
repopath=$(pwd)
#git-editing
read -p "edit repo? y/enter: " edit
while [ $edit = y ]; do
clear; read -p "add | remove | a/r/enter: " option
##add
while [ $option = a ]; do
	clear; read -p "add all or individual files? a/i/enter: " add
	if [ $add = a ]; then
		git add .
		git status
		read -p "press enter to continue "
	fi
	if [ $add = i ]; then
		clear; ls -A
		read -p "add directory | change directory | exit | a/c/enter: " dir
		while [ $dir = a ]; do
			clear; ls -A; read -p "add which directory: " dira
			clear; read -p "is the directory outside of the repo? y/enter: " dirout
			if [ $dirout = y ]; then
				sudo cp -a $dira .
				read -p "what is the directory named: " dira
			fi
			git add $dira
			git status
			read -p "add another directory? a/enter: " dir
		done
		while [ $dir = c ]; do
			clear; ls -A; read -p "change to which directory: " dirc
			cd $dirc
			ls -A
			read -p "change directory? c/enter: " dir
		done
		if [ ! $dir = a ] && [ ! $dir = c ]; then
			break
		fi
	fi
clear; read -p "add | remove | a/r/enter: " option
done
##remove
while [ $option = r ]; do
	clear; ls -A
	read -p "remove directory | change directory | exit | r/c/enter: " dir
	while [ $dir = r ]; do
		clear; ls -A; read -p "remove which directory: " dirr
		git rm -r $dirr
		sudo rm -r $dirr
		git status
		read -p "remove another directory? r/enter" dir
	done
	while [ $dir = c ]; do
		clear; ls -A; read -p "change to which directory: " dirc
		cd $dirc
		ls -A
		read -p "change directory? c/enter: " dir
	done
	if [ ! $dir = r ] && [ ! $dir = c ]; then
		break
	fi
clear; read -p "add | remove | a/r/enter: " option
done
#repairs
##edit
if [ ! $edit=y ]; then
	edit=y
fi
##invalid-option
if [ ! $option = a ] && [ ! $option = r ]; then
	read -p "'$option' is not a valid option | press enter to continue "
	exit
fi
read -p "choose another option? y/enter: " edit
done
#sync
cd $repopath
read -p "sync changes with git repo? (enter = yes) n/enter: " sync
if [ $sync = n ]; then
	clear; read -p "are you sure you do not want to sync? y/enter: " sync
	if [ ! $sync = y ]; then
		sync=b
	else
		sync=c
	fi
fi
if [ ! $sync = n ] && [ ! $sync = c ]; then
	clear; read -p "enter commit message" commit
	git commit -m "commit"
	git remote add origin https://gitlab.com/$user/$repo.git
	git push origin $branch
	read -p "did everything sync right? press enter to continue "
fi
sudo rm -r .git
