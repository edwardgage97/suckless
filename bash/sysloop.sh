#!/bin/bash
clear

#variables
line="==================="
menu="    Backup Menu    "

echo "
$line
$menu System-Backup
$line
"
read -p "do you want to backup to another location? type the exact file path or type no: " path
if [ $path = no ]; then
	clear; exit 0
else
	sudo mkdir -p $path
	sudo cp -au ~/.bashrc ~/bash; sudo rsync -ar ~/bash $path/sys
	sudo rsync -ar ~/.dwm $path/sys
	~/bash/sysloop.sh
fi
