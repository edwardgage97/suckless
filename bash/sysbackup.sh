#!/bin/bash
clear

#VARIABLES
line="==================="
menu="    Backup Menu    "
fmenu='
$line
$menu
$line
'
#BASH
echo "backing up ~/.bashrc and ~/.bash"
sudo rsync -a ~/.bashrc ~/bash
sudo rsync -a ~/bash /media/MS/backups/sys /media/NSB/backups/sys
clear

#DWM
echo "backing up ~/.dwm"
sudo rsync -a ~/.dwm /media/MS/backups/sys /media/NSB/backups/sys
clear

#OTHER
echo "
$line
$menu System-Backup
$line
"
read -p "do you want to backup to another location? type the exact file path or type no: " path
if [ $path = no ]; then
	clear; exit 0
else
	sudo mkdir -p $path
	sudo cp -au ~/.bashrc ~/bash; sudo rsync -ar ~/bash $path
        sudo rsync -ar ~/.dwm $path
	~/bash/sysloop.sh
fi
