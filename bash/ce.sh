#!/bin/sh

#app-id
clear && read -p "!#! NOTICE: ONLY CHEAT ENGINE 7.1 WORKS WITH WINE/PROTON !#! | press enter to continue: "
clear && read -p "enter steam app id: " id
clear && read -p "confirm: enter steam app id: " id_2
while [ ! $id = $id_2 ]; do
	clear && read -p "!#! STEAM APP ID'S DO NOT MATCH !#! | press enter to continue: "
	clear && read -p "enter steam app id: " id
	clear && read -p "confirm: enter steam app id: " id_2
done
#debug-launch-option
if [ -e /tmp/proton_$(whoami) ]; then
	$(cat /tmp/proton_$(whoami)/run | grep -o $id)
	if [ $(echo $?) = 0 ]; then
		clear && read -p "add PROTON_DUMP_DEBUG_COMMANDS=1 %command% to $id's launch option and launch the game | press enter to continue: "
		exit
	fi
else
	clear && read -p "add PROTON_DUMP_DEBUG_COMMANDS=1 %command% to $id's launch option and launch the game | press enter to continue: "
fi
#run-or-install
if [ -e "/home/$(whoami)/.steam/steam/steamapps/compatdata/$id/pfx/drive_c/Program Files/Cheat Engine 7.1/Cheat Engine.exe" ]; then
	clear
	/tmp/proton_$(whoami)/run "/home/$(whoami)/.steam/steam/steamapps/compatdata/$id/pfx/drive_c/Program Files/Cheat Engine 7.1/Cheat Engine.exe"
else
	clear && read -p "install cheat engine: y/n: " install
	if [ $install = y ]; then
		clear && read -p "enter cheat engine installer directory (ex: ~/downloads/Cheat Engine 71.exe): " dir
		while [ ! $(ls ~/ | grep .wine) = '.wine' ]; do
			doas pacman -Syu && doas pacman -S wine
		done
		/tmp/proton_$(whoami)/run "$dir"
		/tmp/proton_$(whoami)/run "/home/$(whoami)/.steam/steam/steamapps/compatdata/$id/pfx/drive_c/Program Files/Cheat Engine 7.1/Cheat Engine.exe"
	fi
fi
