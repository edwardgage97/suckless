##Skel
#
# ~/.bashrc
#

#If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias ls='ls --color=auto'
# PS1='[\u@\h \W]$ '

##Custom
#maintenance
alias xfce='startxfce4'
alias dwm='startx'
alias fsu='doas pacman -Syu'
alias clean='doas pacman -Syu && doas mv ~/.cache/mesa_shader_cache ~/; doas rm -r ~/.cache && mkdir -m 700 ~/.cache && chown $(whoami):$(whoami) ~/.cache && doas mv ~/mesa_shader_cache ~/.cache'
alias fclean='doas pacman -Syu && yes " " | doas pacman -Scc && doas mv ~/.cache/mesa_shader_cache ~/; doas rm -r ~/.cache && mkdir -m 700 ~/.cache && chown $(whoami):$(whoami) ~/.cache && doas mv ~/mesa_shader_cache ~/.cache'
alias grupdate='doas grub-mkconfig -o /boot/grub/grub.cfg'
alias grubinstall='doas grub-install --target x86_64-efi --efi-directory=/boot/efi --bootloader=Artix --recheck'
alias grubinstall-bios='doas grub-install --recheck /dev/sda'
alias grubinstall-vm='doas grub-install --recheck /dev/vda'
alias gredit='doas vim /etc/default/grub && grupdate'
alias pwr='doas shutdown -h now'
alias rbt='doas reboot'
alias smci='doas make clean install'
alias smcib='doas make clean install && killall dwm'
alias lsblk='lsblk -o NAME,SIZE,TYPE,STATE,PARTLABEL,MOUNTPOINTS,UUID'
#convenience
alias svim='doas vim'
alias s='doas'
alias gparted='doas gparted'
alias ls='ls --color=auto -A'
alias lsgpu='lspci -vnn'
alias bashrc='vim ~/.bashrc; bash'
alias bashdir='cd ~/bash'
alias bashclr='doas rm ~/.bash_history'
alias rsync='rsync -aP'
alias cp='cp -a'
alias stopx='pgrep -l dwm | grep -o dwm && killall dwm || killall xfce4-session'
alias notes='vim ~/documents/notes'
alias testsh='~/bash/testsh.sh'
alias ce='~/bash/ce.sh'
alias technic='java -jar ~/downloads/TechnicLauncher.jar'
alias technicdir='cd ~/.technic/modpacks/sodium-119'
alias cal='cal -m'
alias date='date +[%-m/%-d/%Y]-[%T]'
alias luks='doas cryptsetup'
#mounting
alias luksmount='~/bash/mount.sh'
alias umount='doas umount'
alias mount='doas mount'
alias eject='doas eject'
#cd
alias ..='cd ..'
alias root='cd /'
alias home='cd ~/'
alias suckdir='cd ~/.config/suckless'
alias vid='cd ~/videos'
alias pic='cd ~/pictures'
alias down='cd ~/downloads'
alias doc='cd ~/documents'
#resolutions
alias 1080p='xrandr -s 1920x1080'
alias 720p='xrandr -s 1280x720'
alias 480p='xrandr -s 854x480'
alias wide='2560x1080'
#coloring
LS_COLORS='rs=0:fi=00;34:di=01;33:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=00:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.avif=01;35:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:*~=00;90:*#=00;90:*.bak=00;90:*.old=00;90:*.orig=00;90:*.part=00;90:*.rej=00;90:*.swp=00;90:*.tmp=00;90:*.dpkg-dist=00;90:*.dpkg-old=00;90:*.ucf-dist=00;90:*.ucf-new=00;90:*.ucf-old=00;90:*.rpmnew=00;90:*.rpmorig=00;90:*.rpmsave=00;90:no=00;32'
export LS_COLORS
