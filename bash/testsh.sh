#/bin/sh

if [ -e test.sh ]; then
	vim test.sh
	./test.sh
else
	vim test.sh
	chmod 777 test.sh
	./test.sh
fi
