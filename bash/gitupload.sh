#!/bin/sh
clear; read -p "enter git email: " email
git config --global user.email "$email"
cd ~/.config/suckless
doas rm -r bash .git; doas cp -a ~/.bashrc ~/bash; doas cp -a ~/bash .
doas rm -r browser; mkdir browser; doas cp -a ~/.mozilla browser
doas rm -r files/artix.bashrc files/pacman.conf files/neofetch files/screengrab files/htop files/doas.conf
doas cp -a /etc/bash/bashrc.d/artix.bashrc /etc/pacman.conf /usr/bin/neofetch ~/.config/screengrab ~/.config/htop /etc/doas.conf files
if [ -e /etc/tlp.conf ]; then
	doas rm -r files/tlp.conf && doas cp -a /etc/tlp.conf files
fi
if [ -e ~/.config/xfce4 ]; then
	doas rm -r files/xfce4 && doas cp -a ~/.config/xfce4 files
fi
git init --initial-branch=main
git remote add origin https://gitlab.com/edwardgage97/suckless.git
git add .
git commit -m "Initial commit"
git push -u origin main
