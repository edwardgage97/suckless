#!/bin/sh
exec /usr/bin/pipewire &
exec /usr/bin/pipewire-pulse &
exec /usr/bin/wireplumber &
exec slstatus &
#pic-wallpaper
exec feh --no-fehbg --bg-fill ~/.config/suckless/files/dwmlogo.png &
#vid-wallpaper
#exec wallset ~/.config/suckless/files/dwmlogo.mp4 &
exec picom
#exec thunar --daemon &
#exec thunar-volman
