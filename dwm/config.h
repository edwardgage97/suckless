/* constants */
#include <X11/XF86keysym.h>
#define TERMINAL "st"
#define TERMCLASS "st"
/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappx	    = 0;	/* gaps between windows */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Terminus:size=15", "twemoji:size=15" };
static const char dmenufont[]       = { "Terminus:size=15" };
/* default colors */
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
/* custom colors */
static const char col_cyan2[]	    = "#00cdcd";
static const char col_black[]	    = "#000000";
static const char col_darkgray[]    = "#131313";
static const char col_white[]	    = "#ffffff";
static const char *colors[][3]      = {
	/*                  fg         bg         border   */
	[SchemeNorm]	= { col_gray3, col_black, col_black },
	[SchemeSel]	= { col_gray4, col_black, col_cyan },
	[SchemeStatus]	= { col_gray3, col_black, "#000000" }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]	= { col_white, col_darkgray, "#000000" }, // Tagbar left selected {text,background,not used but cannot be empty}
	[SchemeTagsNorm]= { col_cyan, col_black, "#000000" }, // Tagbar left unselected {text,background,not used but cannot be empty}
	[SchemeInfoSel]	= { col_white, col_black, "#000000" }, // infobar middle  selected {text,background,not used but cannot be empty}
	[SchemeInfoNorm]= { col_gray3, col_black, "#000000" }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

/* tagging */
static const char *tags[] = { "I", "II", "III" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "Gimp",    NULL,     NULL,           0,         1,          0,           0,        -1 },
	{ "Firefox", NULL,     NULL,           1 << 8,    0,          0,          -1,        -1 },
	{ "st",      NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ NULL,      NULL,     "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */
static const int mainmon = 0; /* xsetroot will only change the bar on this monitor */
#include "layouts.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[T]",      tile },    /* first entry is default */
	{ "[F]",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "[G]",      grid },
	{ "[C]",      tcl },
	{ "[B]",      bstack },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask|Mod1Mask,	KEY,	  swaptags,	  {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* Default Commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_black, "-nf", col_gray3, "-sb", col_darkgray, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *htopcmd[]  = { "st", "htop", NULL };
static const char scratchpadname[] = "scratchpad";
//laptop 80x24 | desktop 120x34 
static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-g", "120x34", NULL };

//Custom Commands
/* Screenshot */
static const char *sg[] = {"screengrab", NULL};
/* Screenlock */
static const char *lock[] = { "slock", NULL};
/* Brightness */
static const char *brightu[] = { "brightnessctl", "s", "+5%", NULL};
static const char *brightd[] = { "brightnessctl", "s", "5%-", NULL};
/* Volume */
static const char *volu[] = { "pactl", "set-sink-volume", "0", "+5%", NULL};
static const char *vold[] = { "pactl", "set-sink-volume", "0", "-5%", NULL};
static const char *mute[] = { "pactl", "set-sink-mute", "0", "toggle", NULL};
/* Compositor */
static const char *picomoff[] = { "pkill", "picom", NULL };
static const char *picomon[] = { "picom", NULL };

//Binds
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,		        XK_s,      spawn,          {.v = dmenucmd } },
	{ MODKEY,	                XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,		XK_Return, spawn,          {.v = htopcmd } },
	{ MODKEY,			XK_grave,  togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY|ShiftMask,             XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,      inplacerotate,  {.i = +1} },
	{ MODKEY|ShiftMask,             XK_k,      inplacerotate,  {.i = -1} },
	{ MODKEY|ShiftMask,             XK_h,      inplacerotate,  {.i = +2} },
	{ MODKEY|ShiftMask,             XK_l,      inplacerotate,  {.i = -2} },
	{ MODKEY|ShiftMask,	        XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,	        XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,	                XK_z,      zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,	                XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,			XK_g,	   setlayout,	   {.v = &layouts[3]} },
	{ MODKEY,			XK_c,	   setlayout,	   {.v = &layouts[4]} },
	{ MODKEY,			XK_b,      setlayout,	   {.v = &layouts[5]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	//TAGKEYS(                        XK_4,                      3)
	//TAGKEYS(                        XK_5,                      4)
	{ MODKEY|ShiftMask,             XK_Escape,      quit,           {0} },
	
	//Custom Command Binds
	/* Screenlock (slock) */
	{ MODKEY,		        XK_Escape, spawn,	   {.v = lock } },
	/* Screenshot (screengrab) */
	{ 0,				XK_Print,  spawn,	   {.v = sg } },
	/* Brightness (brightnessctl) */
	{ 0, 				XF86XK_MonBrightnessUp,	  	   spawn,	   { .v = brightu} },
	{ 0,				XF86XK_MonBrightnessDown,	   spawn, 	   { .v = brightd} },
	/* Volume (pactl) */
	{ 0,				XF86XK_AudioRaiseVolume,	   spawn, 	   { .v = volu} },
	{ 0,				XF86XK_AudioLowerVolume, 	   spawn,	   { .v = vold} },
	{ MODKEY,			XF86XK_AudioMute, 		   spawn, 	   { .v = mute} },
	/* Compositor (picom) */
	{ MODKEY|ShiftMask|Mod1Mask,	XK_p,	   spawn, 	   {.v = picomoff } },	
	{ MODKEY|ShiftMask,		XK_p,	   spawn,	   {.v = picomon } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
